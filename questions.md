1. what is programming?
Programming is a way to “instruct the computer to perform various tasks”.
Confusing? Let us understand the definition deeply.
“Instruct the computer”: this basically means that you provide the computer a set of instructions that are written in a language that the computer can understand.

1.1. how was programming invented?
The first programming language was developed in 1883 when Ada Lovelace and Charles Babbage worked together on the Analytical Engine

1.1.1. what is the first programming language?
Computer Programming History: FORTRAN was the first computer programming language that was widely used. A Brief History of Computer Programming Languages: Computer code is the foundation of computers, enabling them to do the tasks humans need them to do.

1.1.1.1. where is fortran used?
1.1.1.2. how was fortran made?
1.1.1.3. what tasks do humans need computers to do?

1.1.2. who is ada lovelace?
Augusta Ada King, Countess of Lovelace (née Byron; 10 December 1815 – 27 November 1852) was an English mathematician and writer, chiefly known for her work on Charles Babbage's proposed mechanical general-purpose computer, the Analytical Engine. She was the first to recognise that the machine had applications beyond pure calculation, and to have published the first algorithm intended to be carried out by such a machine. As a result, she is often regarded as the first computer programmer.

1.1.2.1. what is a countess?
1.1.2.2. what is a mathematician?
1.1.2.3. what is mechanical?
1.1.2.4. what is a computer programmer?
1.1.2.5. what is lovelace?

1.1.3. who is charles babbage?
Charles Babbage, (born December 26, 1791, London, England—died October 18, 1871, London), English mathematician and inventor who is credited with having conceived the first automatic digital computer.

1.1.3.1. what is digital?
1.1.3.2. what is an inventor?
1.1.3.3. what is a computer?

1.1.4. what is the analytical engine?
The Analytical Engine was a proposed mechanical general-purpose computer designed by English mathematician and computer pioneer Charles Babbage.[2][3] It was first described in 1837 as the successor to Babbage's difference engine, which was a design for a simpler mechanical calculator.[4]

The Analytical Engine incorporated an arithmetic logic unit, control flow in the form of conditional branching and loops, and integrated memory, making it the first design for a general-purpose computer that could be described in modern terms as Turing-complete.[5][6] In other words, the logical structure of the Analytical Engine was essentially the same as that which has dominated computer design in the electronic era.[3] The Analytical Engine is one of the most successful achievements of Charles Babbage. 

1.1.4.1. what is the meaning of general-purpose?
1.1.4.2. what is a pioneer?
1.1.4.3. what is the difference engine?
1.1.4.4. what is an arithmetic logic unit?
1.1.4.5. what is control flow?
1.1.4.6. what are loops?
1.1.4.7. what is integrated memory?
1.1.4.8. what is turing-complete?

1.2. why was programming invented?
Lovelace was able to discern the importance of numbers, realizing that they could represent more than just numerical values of things. Lovelace wrote an algorithm for the Analytical Engine, the first computer program, to compute Bernoulli numbers.

1.2.1. what is the importance of numbers?
Numbers are important. Whether costs, revenues, performance, targets – most people agree that numbers are important. Interpretation of these numbers is key; the numbers can influence decisions related to performance, investments and effectiveness among other things

1.2.1.1. what are costs?
1.2.1.2. what are revenues?
1.2.1.3. what is performance?
1.2.1.4. what are targets?
1.2.1.5. what are decisions?
1.2.1.6. what are investments?
1.2.1.7. what is effectiveness?

1.2.2. what can numbers represent?
A number is a mathematical object used to count, measure, and label. The original examples are the natural numbers 1, 2, 3, 4, and so forth. Numbers can be represented in language with number words.

1.2.2.1. what are mathematical objects?
1.2.2.2. what is measuring?
1.2.2.3. what is labeling?
1.2.2.4. what are the different types of numbers?

1.2.3. what is an algorithm?
An algorithm is a set of instructions for solving a problem or accomplishing a task. One common example of an algorithm is a recipe, which consists of specific instructions for preparing a dish or meal.

1.2.3.1. what are the different types of algorithms?
1.2.3.2. wht are instructions?
1.2.3.3. how to calculate what algorithm is the best?

1.2.4. what are bernoulli numbers?
In mathematics, the Bernoulli numbers Bn are a sequence of rational numbers which occur frequently in analysis. The Bernoulli numbers appear in (and can be defined by) the Taylor series expansions of the tangent and hyperbolic tangent functions, in Faulhaber's formula for the sum of m-th powers of the first n positive integers, in the Euler–Maclaurin formula, and in expressions for certain values of the Riemann zeta function.

The values of the first 20 Bernoulli numbers are given in the adjacent table. Two conventions are used in the literature, denoted here by B n − {\displaystyle B_{n}^{-{}}} {\displaystyle B_{n}^{-{}}} and B n + {\displaystyle B_{n}^{+{}}} {\displaystyle B_{n}^{+{}}}; they differ only for n = 1, where B 1 − = − 1 / 2 {\displaystyle B_{1}^{-{}}=-1/2} {\displaystyle B_{1}^{-{}}=-1/2} and B 1 + = + 1 / 2 {\displaystyle B_{1}^{+{}}=+1/2} {\displaystyle B_{1}^{+{}}=+1/2}. For every odd n > 1, Bn = 0. For every even n > 0, Bn is negative if n is divisible by 4 and positive otherwise. The Bernoulli numbers are special values of the Bernoulli polynomials B n ( x ) {\displaystyle B_{n}(x)} B_{n}(x), with B n − = B n ( 0 ) {\displaystyle B_{n}^{-{}}=B_{n}(0)} {\displaystyle B_{n}^{-{}}=B_{n}(0)} and B n + = B n ( 1 ) {\displaystyle B_{n}^{+}=B_{n}(1)} {\displaystyle B_{n}^{+}=B_{n}(1)}.[

1.2.4.1. what is mathematics?
1.2.4.2. what are rational numbers?
1.2.4.3. what is taylor series?
1.2.4.4. what is a tangent?
1.2.4.5. what is a hyperbolic tangent?
1.2.4.6. what is the faulhabers formula?
1.2.4.7. what is the euler-maclaurin formula?
1.2.4.8. what is the riemann zeta function?
1.2.4.9. what is analysis in math?

1.3. when was programming invented?
in 1883

1.3.1. what happened in 1883?
answering this any further would derail this document too much

1.3.2. why was 1883 important?
answering this any further would derail this document too much

1.3.3. which scientific discoveries were made in 1883?
Chemistry

    Svante Arrhenius develops ion theory to explain conductivity in electrolytes.[1][2]
    The Claus process is first patented by German chemist Carl Friedrich Claus.[3]
    The Schotten–Baumann reaction is first described by chemists Carl Schotten and Eugen Baumann.

Earth sciences

    August 26 – Krakatoa begins its final phase of eruptions at 1:06 pm local time. These produce a number of tsunami, mainly in the early hours of the next day, which result in about 36,000 deaths on the islands of Sumatra and Java. The final explosion at 10:02 am on August 27 destroys the island of Krakatoa itself and is heard up to 3000 miles away.
    Vasily Dokuchaev publishes Russian Chernozem.

Genetics

    The concept and term Eugenics are formulated by Francis Galton.[4]

Medicine

    Thomas Clouston publishes Clinical Lectures on Mental Diseases.
    Emil Kraepelin publishes Compendium der Psychiatrie.
    Journal of the American Medical Association first published under this title.

Microbiology

    Robert Koch isolates Vibrio cholerae, the cholera bacillus.

Physics

    Osborne Reynolds popularizes use of the Reynolds number in fluid mechanics.[5][6]

Technology

    May 24 – Brooklyn Bridge opens to traffic in New York. Designed by John A. Roebling with project management assisted by his wife Emily, its main suspension span of 1,595 feet 6 inches (486.31 m) exceeds the previous record by 330 feet (100 m), and will not be surpassed for twenty years.
    Charles Fritts constructs the first solar cell using the semiconductor selenium on a thin layer of gold to form a device giving less than 1% efficiency.

1.3.3.1. what is a reynolds number?
1.3.3.2. what is fluid mechanics?
1.3.3.3. how does a solar cell function?
1.3.3.4. what is a semiconductor?
1.3.3.5. what is cholera bacillus?
1.3.3.6. what is eugenics?
1.3.3.7. what is ion theory?
1.3.3.8. what is the clauss process?
1.3.3.9. what is The Schotten–Baumann reaction?

2. why do we need programming?
The reason that programming is so important is that it directs a computer to complete these commands over and over again, so people do not have to do the task repeatedly. Instead, the software can do it automatically and accurately.

2.1. why are machines so good at repetition?
Robots cost less to maintain over time, they’re efficient and commit fewer errors than humans, and they never throw a tantrum. These are all valid company considerations (at least from a business perspective), and they always go with what is better for their bottom line.

2.1.1. what is a robot?
robot, any automatically operated machine that replaces human effort, though it may not resemble human beings in appearance or perform functions in a humanlike manner. By extension, robotics is the engineering discipline dealing with the design, construction, and operation of robots.

2.1.1.1. what is a human being?
2.1.1.2. what is an extension?
2.1.1.3. what is robotics?
2.1.1.4. what are the different disciplines of engineering?

2.1.2. what is a bottom line?
The bottom line refers to a company's earnings, profit, net income, or earnings per share (EPS). The reference to the bottom line describes the relative location of the net income figure on a company's income statement.

2.1.2.1. what is a company?
2.1.2.2. what are earnings?
2.1.2.3. what is profit?
2.1.2.4. what is net income?
2.1.2.5. what is EPS?
2.1.2.6. what is an income statement?

2.1.3. what is a business perspective?
The Business Perspective defines the business level view of the system. The various viewpoints defined show how people (actors) interact with processes at various locations within the business, and the things they handle and use (business entities).

2.1.3.1. what are locations in within a business?
2.1.3.2. what are the different business entities?
2.1.3.3. what is a business level?

2.1.4. what are company considerations?
In business, consideration means one of two things. It's either synonymous with compensation, or it is a contractual exchange of mutual promises that benefit both parties in a contract. That second definition is the one we are discussing today. Consideration in this sense has to have tangible value.

2.1.4.1. what is compensation?
2.1.4.2. what is a contract?
2.1.4.3. what is tangible value?

2.1.5. when do robots commit errors?
//TODO: answer this later

2.2. are machines flawless?
A machine cannot be 100 percent efficient because output of a machine is always less than input. A certain amount of work done on a machine is lost to overcome friction and to lift some moving parts of the machine.

2.2.1. what is a machine?
A machine is a physical system using power to apply forces and control movement to perform an action. The term is commonly applied to artificial devices, such as those employing engines or motors, but also to natural biological macromolecules, such as molecular machines. Machines can be driven by animals and people, by natural forces such as wind and water, and by chemical, thermal, or electrical power, and include a system of mechanisms that shape the actuator input to achieve a specific application of output forces and movement. They can also include computers and sensors that monitor performance and plan movement, often called mechanical systems. 

2.2.1.1. what is a physical system?
2.2.1.2. what is artificial?
2.2.1.3. what is biological?
2.2.1.4. what are macromolecules?
2.2.1.5. what are molecular machines?
2.2.1.6. what is a mechanism?

2.2.2. how is efficiency calculated?
How Do You Calculate Efficiency? Efficiency can be expressed as a ratio by using the following formula: Output ÷ Input. Output, or work output, is the total amount of useful work completed without accounting for any waste and spoilage. You can also express efficiency as a percentage by multiplying the ratio by 100.

2.2.2.1. what is a ratio?
2.2.2.2. what is output?
2.2.2.3. what is input?

2.2.3. what is friction?
The force of static friction FsF_sFs​F, start subscript, s, end subscript is a force between two surfaces that prevents those surfaces from sliding or slipping across each other. This is the same force that allows you to accelerate forward when you run. Your planted foot can grip the ground and push backward, which causes the ground to push forward on your foot. We call this "grippy" type of friction, where the surfaces are prevented from slipping across each other, a static frictional force. If there were absolutely no friction between your feet and the ground, you would be unable to propel yourself forward by running, and would simply end up jogging in place (similar to trying to run on very slippery ice).

2.2.3.1. what is static?
2.2.3.2. what is a force?


2.3. what are machines bad at?
empathy, looking a person in the eye, delievering bad news well, establishing trust and comedy

2.3.1. what is empathy?
the action of understanding, being aware of, being sensitive to, and vicariously experiencing the feelings, thoughts, and experience of another of either the past or present without having the feelings, thoughts, and experience fully communicated in an objectively explicit manner

2.3.2. why is looking a person in the eye important?
It's easy to have misunderstandings, even when two people believe they're both listening intently. Making eye contact helps both people focus on the conversation and read facial expressions. This can improve understanding. And improving understanding can significantly improve communication between two people.

2.3.3. how to deliever bad news well?
How To Deliver Bad News To Anyone

    Make eye contact. As cliche as it sounds, it's better for the receiving party to be sitting down. ...
    Sort yourself out first. It's never good to give someone bad news while you're upset. ...
    Try to be neutral. ...
    Be prepared. ...
    Speak at the level you need to. ...
    Use facts. ...
    Don't negotiate. ...
    Offer help.

2.3.4. how to establish trust?
Ten of the most effective ways to build trust

    Value long-term relationships. Trust requires long-term thinking. ...
    Be honest. ...
    Honor your commitments. ...
    Admit when you're wrong. ...
    Communicate effectively. ...
    Be vulnerable. ...
    Be helpful. ...
    Show people that you care.

2.3.5. how to perform comedy?
not getting more into it here since this is a skill for a separate project

3. what types of programming languages exist?
Procedural Programming Language
Functional Programming Language
Scripting Programming Language
Logic Programming Language
Object-Oriented Programming Language

3.1. what is the purpose of procedural programming?
The focus of procedural programming is to break down a programming task into a collection of variables, data structures, and subroutines, whereas in object-oriented programming it is to break down a programming task into objects that expose behavior (methods) and data (members or attributes) using interfaces.

3.1.1. what are the best languages to do procedural programming in?
C provides the basis for the C++ procedural programming paradigm. Procedural programming involves using variables and functions to create reusable procedures that constitute a full program. This part of the book introduces you to the features provided in C++ that would allow you to write fully procedural programs.

3.1.1.1. where can I learn c?
3.1.1.2. where can I learn c++?
3.1.1.3. what is procedural?
3.1.1.4. what are variables?

3.1.2. what is a variable?
A variable is any characteristics, number, or quantity that can be measured or counted. A variable may also be called a data item. Age, sex, business income and expenses, country of birth, capital expenditure, class grades, eye colour and vehicle type are examples of variables. It is called a variable because the value may vary between data units in a population, and may change in value over time. 

3.1.3. what is a data structure?
In computer science, a data structure is a data organization, management, and storage format that enables efficient access and modification.[1][2][3] More precisely, a data structure is a collection of data values, the relationships among them, and the functions or operations that can be applied to the data,[4] i.e., it is an algebraic structure about data. 

3.1.4. what is a subroutine?
In computer programming, a subroutine is a sequence of program instructions that performs a specific task, packaged as a unit. This unit can then be used in programs wherever that particular task should be performed.

Subroutines may be defined within programs, or separately in libraries that can be used by many programs. In different programming languages, a subroutine may be called a routine, subprogram, function, method, or procedure. Technically, these terms all have different definitions, and the nomenclature varies from language to language. The generic, umbrella term callable unit is sometimes used.[1] 

3.2. what is the purpose of functional programming?
A major goal of functional programming is to minimize side effects, which is accomplished by isolating them from the rest of the software code. Separating side effects from the rest of your logic can make a program easier to maintain, test, debug, extend and refactor.

3.2.1. what are the best languages to do functional programming in?
3.2.2. what is logic?
3.2.3. what is software maintaining?
3.2.4. what is software testing?
3.2.5. what is software debugging?
3.2.6. what is software extending?
3.2.7. what is software refactoring?

3.3. what is the purpose of scripting programming?
Scripting languages are types of programming languages where the instructions are written for a run-time environment, to bring new functions to applications, and integrate or communicate complex systems and other programming languages

3.3.1. what is a run-time environment?
3.3.2. what is a function?
3.3.3. what is integration?
3.3.4. what is a system?
3.3.5. what is a complex system?
3.3.6. what are the best languages to do scripting programming in?

3.4. what is the purpose of logic programming?
Logic Programming is a method that computer scientists are using to try to allow machines to reason because it is useful for knowledge representation. In logic programming, logic is used to represent knowledge and inference is used to manipulate it.

3.4.1. what is a method?
3.4.2. what is a computer scientist?
3.4.3. what is knowledge representation?
3.4.4. what are the best languages to do logic programming in?

3.5. what is the purpose of object-oriented programming?
Object-oriented programming aims to implement real-world entities like inheritance, hiding, polymorphism etc in programming. The main aim of OOP is to bind together the data and the functions that operate on them so that no other part of the code can access this data except that function

3.5.1. what is an entity?
3.5.2. what is inheritance?
3.5.3. what is hiding?
3.5.4. what is polymorphism?
3.5.5. what are the best languages to do object-oriented programming in?

sources:
1. https://hackr.io/blog/what-is-programming
1.1., 1.2. and 1.3. https://www.hp.com/us-en/shop/tech-takes/computer-history-programming-languages
1.1.1. https://www.hp.com/us-en/shop/tech-takes/computer-history-programming-languages
1.1.2. https://en.wikipedia.org/wiki/Ada_Lovelace
1.1.3. https://www.britannica.com/biography/Charles-Babbage
1.1.4. https://en.wikipedia.org/wiki/Analytical_Engine
1.2.1. https://reclaystewardedge.com/insights/blog/the-importance-of-numbers/
1.2.2. https://en.wikipedia.org/wiki/Number
1.2.3. https://www.investopedia.com/terms/a/algorithm.asp
1.2.4. https://en.wikipedia.org/wiki/Bernoulli_number
1.3.1. and 1.3.2. https://en.wikipedia.org/wiki/1883_in_the_United_States
1.3.3. https://en.wikipedia.org/wiki/1883_in_science
2. https://www.gcu.edu/blog/engineering-technology/computer-programming-importance
2.1. https://www.techslang.com/robots-taking-over-jobs/
2.1.1. https://www.britannica.com/technology/robot-technology
2.1.2. https://www.investopedia.com/terms/b/bottomline.asp
2.1.3. http://www.unified-am.com/UAM/UAM/guidances/guidelines/uam_business_pers_CBC3D67D.html
2.1.4. https://www.pokalalaw.com/what-does-consideration-mean-in-a-business-contract/
2.2. https://www.topperlearning.com/answer/give-reasons-a-machine-cannot-be-100-percent-efficient/pekflz3pp
2.2.1. https://en.wikipedia.org/wiki/Machine
2.2.2. https://www.investopedia.com/terms/e/efficiency.asp
2.2.3. https://www.khanacademy.org/science/physics/forces-newtons-laws/inclined-planes-friction/a/what-is-friction
2.3. https://www.forbes.com/sites/blakemorgan/2017/08/16/10-things-robots-cant-do-better-than-humans/?sh=7da4ba5c83d5
2.3.1. https://www.merriam-webster.com/dictionary/empathy
2.3.2. https://www.betterup.com/blog/why-is-eye-contact-important
2.3.3. https://www.lifehack.org/articles/communication/how-deliver-bad-news-anyone.html
2.3.4. https://www.betterup.com/blog/how-to-build-trust
3. https://intellipaat.com/community/76833/what-are-the-4-types-of-programming-language
3.1. https://en.wikipedia.org/wiki/Procedural_programming
3.1.1. https://www.oreilly.com/library/view/learn-c-for/9781430264583/9781430264576_Part1.xhtml
3.1.2. https://www.abs.gov.au/websitedbs/D3310114.nsf/home/statistical+language+-+what+are+variables
3.1.3. https://en.wikipedia.org/wiki/Data_structure
3.1.4. https://en.wikipedia.org/wiki/Subroutine
3.2. https://www.keycdn.com/blog/functional-programming
3.3. https://rockcontent.com/blog/scripting-languages/
3.4. http://www.doc.ic.ac.uk/~cclw05/topics1/index.html
3.5. https://www.geeksforgeeks.org/object-oriented-programming-oops-concept-in-java/